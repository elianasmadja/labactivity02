package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter{
    public static void main (String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter a number of your choosing");
        int num = scan.nextInt();
        System.out.println(num);
        num = Utilities.doubleMe(num);
        System.out.println(num);
    }
}